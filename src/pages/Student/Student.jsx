import React, { useState, useEffect } from "react";
import "./Student.css";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";
import useRandomID from "../../hooks/randomID/useRandomID";
import { Button, ButtonGroup } from "@chakra-ui/react";
import { Input } from "@chakra-ui/react";
import { RangeDatepicker } from "chakra-dayzed-datepicker";
// import { useToast } from '@chakra-ui/react'
import {createStudent,listCourse,listStaff,listStudent} from "../../hooks/api/apicalls";
import { useToast } from '@chakra-ui/react'
import { Select } from '@chakra-ui/react'
import moment from "moment";

const Student = () => {
const toast = useToast()
  const [isOpen, setIsOpen] = useState(false);
  const formData = new FormData()

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const [staffList, setStaffList] = useState([])
  const [courseList, setCourseList] = useState([])
  const [courseId, setCourseId] = useState(null)
  const [staffId, setStaffId] = useState(null)
  const [staffDetail, setStaffDetail] = useState(null)
  const [image, setImage] = useState(null)

  const [userData, setUserData] = useState({
    name: "",
    address:"",
    email: "",
    phone: "",
    address: "",
    dateEnrolled:"",
    dateOfBirth:""
  });

  const handleChange = (event) => {
    setUserData({ ...userData, [event.target.name]: event.target.value });
  };

  const handleChangeCourse = (e)=>{
    setCourseId(e.target.value)
  }

  const handleChangeStaff = (e)=>{
    setStaffDetail(e.target.value)
  }

  const handleImageUpload = (e)=>{
    setImage(e.target.files[0]);
  }
  
  console.log(image);

  const handleSubmit = async () => {
    try {
      formData.append("image",image)
      const userDataSchema = {
        name:userData.name,
        email:userData.email,
        phone:userData.phone,
        address:userData.address,
        courseId:courseId,
        staffId:staffDetail,
        studentId: useRandomID(),
        dateEnrolled:userData.dateEnrolled,
        dateOfBirth:userData.dateOfBirth,
        image:image
      };
    
      // formData.append("name",userData.name)
      // formData.append("email",userData.email)
      // formData.append("phone",userData.phone)
      // formData.append("address",userData.address)
      // formData.append("courseId",courseId)
      // formData.append("staffId",staffDetail)
      // formData.append("studentId",useRandomID())
      // formData.append("dateEnrolled",userData.dateEnrolled)
      // formData.append("dateOfBirth",userData.dateOfBirth)

   
  
      const createStaffData = await createStudent(userDataSchema)


      if(!createStaffData.status){
        toast({
          title: 'Eror',
          description: createStaffData.error,
          status: 'error',
          duration: 3000,
          isClosable: true,
        })
        const data = await listStaff()
  setStaffList(data.data);
      }
      
      

if(createStaffData.status){
  toast({
    title: 'Success',
    description: "User Created Successfully",
    status: 'success',
    duration: 3000,
    isClosable: true,
  })
  setIsOpen(false)
}
    } catch (error) {
      toast({
        title: 'Error',
        description: error.message,
        status: 'error',
        duration: 3000,
        isClosable: true,
      })
    }
  };



  useEffect(()=>{
const getData = async()=>{
  const data = await listStudent()
  const listCourseData = await listCourse()
  const listStaffData = await listStaff()
  setStaffId(listStaffData.data)
setCourseList(listCourseData.data)
  setStaffList(data.data);
}
getData()
},[])



  return (
    <div>
      <div className="student-container">
        <h1 className="create_student_title white">Create Student</h1>
        <div className="create-button">
          {isOpen ? (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              BACK
            </Button>
          ) : (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              CREATE
            </Button>
          )}
        </div>
        {isOpen ? (
          <div className="student-table-container">
            <div>
              <label className="white" htmlFor="name">
                Name:
              </label>
              <br />
              <Input
                name="name"
                className="white"
                id="name"
                placeholder="Enter the Name"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="image">
                Upload Image:
              </label>
              <br />
              <Input
              type="file"
                name="image"
                className="white"
                id="image"
                placeholder="Upload Image"
                size="md"
                onChange={handleImageUpload}
              />
            </div>
            <div>
              <label className="white" htmlFor="address">
                Address:
              </label>
              <br />
              <Input
                name="address"
                className="white"
                id="address"
                placeholder="Enter the Address"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="email">
                Email:
              </label>
              <br />
              <Input
                name="email"
                className="white"
                id="email"
                placeholder="Enter the Email"
                type="email"
                size="md"
                onChange={handleChange}
              />
            </div>
            
            <div>
              <label className="white" htmlFor="phone">
                Phone:
              </label>
              <br />
              <Input
                name="phone"
                className="white"
                id="phone"
                placeholder="Enter the Phone"
                type="tel"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="courseId">
                Course ID:
              </label>
              <br />
              <Select onChange={handleChangeCourse} placeholder='Select Course ID' style={{width:'90%',color:'white'}}>
                {
                  courseList?.map((item,i)=>(
<option key={i} style={{color:'black'}} value={item.id}>{item.id}</option>
                  ))
                }
</Select>
            </div>

            <div>
              <label className="white" htmlFor="courseId">
                Staff ID:
              </label>
              <br />
              <Select onChange={handleChangeStaff} placeholder='Select Staff ID' style={{width:'90%',color:'white'}}>
                {
                  staffId?.map((item,i)=>(
<option key={i} style={{color:'black'}} value={item.staffId}>{item.staffId}</option>
                  ))
                }
</Select>
            </div>
           
            <div>
              <label className="white" htmlFor="dateEnrolled">
                Date Of Enrolled:
              </label>
              <br />
              <Input
                name="dateEnrolled"
                className="white"
                id="dateEnrolled"
                placeholder="Select Date and Time"
                size="md"
                type="datetime-local"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="dateOfBirth">
                Date Of Birth:
              </label>
              <br />
              <Input
                name="dateOfBirth"
                className="white"
                id="dateOfBirth"
                placeholder="Select Date and Time"
                size="md"
                type="datetime-local"
                onChange={handleChange}
              />
            </div>
            

            <div className="button-container">
              <Button colorScheme="yellow" onClick={handleSubmit}>
                SUBMIT
              </Button>
            </div>
          </div>
        ) : (
          <div className="student-table-container">
            <TableContainer>
              <Table size="sm">
                <Thead>
                  <Tr>
                    <Th>Si. No.</Th>
                    <Th>Name</Th>
                    <Th>Email</Th>
                    <Th>Profile</Th>
                    <Th>Date of Join</Th>
                    <Th>Phone</Th>
                    <Th>Course Id</Th>
                    <Th>Staff Id</Th>
                  </Tr>
                </Thead>
                <Tbody>
                {
                    staffList?.length!==0?
                    staffList?.map((item,i)=>(
                     <Tr key={i}>
                        <Td>{i+1}</Td>
                        <Td>{item?.name}</Td>
                        <Td>{item?.email}</Td>
                        <Td><img src={item?.image} alt={item?.name} width="50px" height="25px"/></Td>
                        <Td>{moment(item?.dateEnrolled).format("YYYY-MM-DD")}</Td>
                        <Td>{item?.phone}</Td>
                        <Td>{item?.courseId}</Td>
                        <Td>{item?.staffId}</Td>
                        <Td>
                          
                        </Td>
                     </Tr>
                    )):
                    <div style={{textAlign:'center',color:'white'}}>No Data Found!</div>
                 }
                </Tbody>
              </Table>
            </TableContainer>
          </div>
        )}
      </div>
    </div>
  );
};

export default Student;
