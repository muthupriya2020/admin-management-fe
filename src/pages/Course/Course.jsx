import React, { useState, useEffect } from "react";
import "./Course.css";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";
import useRandomID from "../../hooks/randomID/useRandomID";
import { Button, ButtonGroup } from "@chakra-ui/react";
import { Input } from "@chakra-ui/react";
import { RangeDatepicker } from "chakra-dayzed-datepicker";
import { useToast } from '@chakra-ui/react'
import {createCourse,listCourse,editCourse,deleteCourse} from "../../hooks/api/apicalls";
import moment from 'moment'

const Student = () => {
  const toast = useToast()
  const [isOpen, setIsOpen] = useState(false);
  const [courseList, setCourseList] = useState([])
  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const [userData, setUserData] = useState({
    name: "",
    description: "",
    instructor: "",
    date: "",
    credits: "",
    id: "",
    fee: "",
    topics: "",
  });

  const handleChange = (event) => {
    setUserData({ ...userData, [event.target.name]: event.target.value });
  };

  const handleSubmit = async () => {
    try {
        const userDataSchema = {
            name:userData.name,
            description:userData.description,
            instructor:userData.instructor,
            date:userData.date,
            credits:userData.credits,
            id: useRandomID(),
            duration: selectedDates,
            fee:userData.fee,
            topics:userData.topics,
          };
      
          const courseData = await createCourse(userDataSchema)
          if(courseData.status){
            toast({
                title: 'Success',
                description: "User Created Successfully",
                status: 'success',
                duration: 3000,
                isClosable: true,
              })
              setIsOpen(false)
          }
       
    } catch (error) {
        toast({
            title: 'Error',
            description: error.message,
            status: 'error',
            duration: 3000,
            isClosable: true,
          })
    }
   
  };

  useEffect(()=>{

    const getData = async()=>{
        const data = await listCourse()
        setCourseList(data.data);
    }
    getData()
  },[])



  return (
    <div>
      <div className="student-container">
        <h1 className="create_student_title white">Create Course</h1>
        <div className="create-button">
          {isOpen ? (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              BACK
            </Button>
          ) : (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              CREATE
            </Button>
          )}
        </div>
        {isOpen ? (
          <div className="student-table-container">
            <div>
              <label className="white" htmlFor="name">
                Name:
              </label>
              <br />
              <Input
                name="name"
                className="white"
                id="name"
                placeholder="Enter the Name"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="description">
                Description:
              </label>
              <br />
              <Input
                name="description"
                className="white"
                id="description"
                placeholder="Enter the Description"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="topics">
                Topics:
              </label>
              <br />
              <Input
                name="topics"
                className="white"
                id="topics"
                placeholder="Enter the Topics"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="instructor">
                Instructor:
              </label>
              <br />
              <Input
                name="instructor"
                className="white"
                id="instructor"
                placeholder="Enter the Instructor"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="credits">
                Credits:
              </label>
              <br />
              <Input
                name="credits"
                className="white"
                id="credits"
                placeholder="Enter the Credits"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="name">
                Fees:
              </label>
              <br />
              <Input
                name="fee"
                className="white"
                id="fees"
                placeholder="Enter the Fees"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="date">
                Date:
              </label>
              <br />
              <Input
                name="date"
                className="white"
                id="date"
                placeholder="Select Date and Time"
                size="md"
                type="datetime-local"
                onChange={handleChange}
              />
            </div>
            <div className="blue">
              <label className="white" htmlFor="date">
                Date:
              </label>
              <br />
              <RangeDatepicker
                name="duration"
                selectedDates={selectedDates}
                onDateChange={setSelectedDates}
              />
            </div>

            <div className="button-container">
              <Button colorScheme="yellow" onClick={handleSubmit}>
                SUBMIT
              </Button>
            </div>
          </div>
        ) : (
          <div className="student-table-container">
            <TableContainer>
              <Table size="sm">
                <Thead>
                  <Tr>
                    <Th>Si. No.</Th>
                    <Th>Name</Th>
                    <Th >Description</Th>
                    <Th>Topics</Th>
                    <Th>Instructor</Th>
                    <Th>Credits</Th>
                    <Th>Fees</Th>
                    <Th>From</Th>
                    <Th>To</Th>
                    {/* <Th>Action</Th> */}
                  </Tr>
                </Thead>
                <Tbody>
                 {
                    courseList?.length!==0?
                    courseList?.map((item,i)=>(
                     <Tr key={i}>
                        <Td>{i+1}</Td>
                        <Td>{item.name}</Td>
                        <Td>{item.description}</Td>
                        <Td>{item.topics}</Td>
                        <Td>{item.instructor}</Td>
                        <Td>{item.credits}</Td>
                        <Td>{item.fee}</Td>
                        <Td>{moment(item.duration[0]).format("YYYY-MM-DD")}</Td>
                        <Td>{moment(item.duration[1]).format("YYYY-MM-DD")}</Td>
                        {/* <Td>
                          
                        </Td> */}
                     </Tr>
                    )):
                    <div style={{textAlign:'center',color:'white'}}>No Data Found!</div>
                 }
                </Tbody>
              </Table>
            </TableContainer>
          </div>
        )}
      </div>
    </div>
  );
};

export default Student;
