import React, { useState, useEffect } from "react";
import "./Staff.css";
import {
  Table,
  Thead,
  Tbody,
  Tfoot,
  Tr,
  Th,
  Td,
  TableCaption,
  TableContainer,
} from "@chakra-ui/react";
import useRandomID from "../../hooks/randomID/useRandomID";
import { Button, ButtonGroup } from "@chakra-ui/react";
import { Input } from "@chakra-ui/react";
import { RangeDatepicker } from "chakra-dayzed-datepicker";
// import { useToast } from '@chakra-ui/react'
import {createStaff,listCourse,listStaff} from "../../hooks/api/apicalls";
import { useToast } from '@chakra-ui/react'
import { Select } from '@chakra-ui/react'

const Student = () => {
const toast = useToast()
  const [isOpen, setIsOpen] = useState(false);

  const [selectedDates, setSelectedDates] = useState([new Date(), new Date()]);

  const [staffList, setStaffList] = useState([])
  const [courseList, setCourseList] = useState([])
  const [courseId, setCourseId] = useState(null)

  const [userData, setUserData] = useState({
    name: "",
    email: "",
    salary: "",
    dateOfJoin: "",
    phone: "",
    staffId: "",
  });

  const handleChange = (event) => {
    setUserData({ ...userData, [event.target.name]: event.target.value });
  };

  const handleChangeCourse = (e)=>{
    setCourseId(e.target.value)
  }


  const handleSubmit = async () => {

    try {
     
      const userDataSchema = {
        name:userData.name,
        email:userData.email,
        salary:userData.salary,
        dateOfJoin:userData.dateOfJoin,
        phone:userData.phone,
        staffId: useRandomID(),
        courseId:courseId,
      };

   
  
      const createStaffData = await createStaff(userDataSchema)

console.log(createStaffData);
      if(!createStaffData.status){
        toast({
          title: 'Eror',
          description: createStaffData.error,
          status: 'error',
          duration: 3000,
          isClosable: true,
        })
        const data = await listStaff()
  setStaffList(data.data);
      }
      
      

if(createStaffData.status){
  toast({
    title: 'Success',
    description: "User Created Successfully",
    status: 'success',
    duration: 3000,
    isClosable: true,
  })
  setIsOpen(false)
}
    } catch (error) {
      toast({
        title: 'Error',
        description: error.message,
        status: 'error',
        duration: 3000,
        isClosable: true,
      })
    }
  };



  useEffect(()=>{
const getData = async()=>{
  const data = await listStaff()
  const listCourseData = await listCourse()
setCourseList(listCourseData.data)
  setStaffList(data.data);
}
getData()
},[])

  return (
    <div>
      <div className="student-container">
        <h1 className="create_student_title white">Create Staff</h1>
        <div className="create-button">
          {isOpen ? (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              BACK
            </Button>
          ) : (
            <Button colorScheme="blue" onClick={() => setIsOpen(!isOpen)}>
              CREATE
            </Button>
          )}
        </div>
        {isOpen ? (
          <div className="student-table-container">
            <div>
              <label className="white" htmlFor="name">
                Name:
              </label>
              <br />
              <Input
                name="name"
                className="white"
                id="name"
                placeholder="Enter the Name"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="email">
                Email:
              </label>
              <br />
              <Input
                name="email"
                className="white"
                id="email"
                placeholder="Enter the Email"
                type="email"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="salary">
                Salary:
              </label>
              <br />
              <Input
                name="salary"
                className="white"
                id="salary"
                placeholder="Enter the Salary"
                type="number"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="phone">
                Phone:
              </label>
              <br />
              <Input
                name="phone"
                className="white"
                id="phone"
                placeholder="Enter the Phone"
                type="tel"
                size="md"
                onChange={handleChange}
              />
            </div>
            <div>
              <label className="white" htmlFor="courseId">
                Course ID:
              </label>
              <br />
              
            
              <Select onChange={handleChangeCourse} placeholder='Select Course ID' style={{width:'90%',color:'white'}}>
                {
                  courseList?.map((item,i)=>(
<option style={{color:'black'}} value={item.id}>{item.id}</option>
                  ))
                }
</Select>
            </div>
           
            <div>
              <label className="white" htmlFor="dateOfJoin">
                Date Of Join:
              </label>
              <br />
              <Input
                name="dateOfJoin"
                className="white"
                id="dateOfJoin"
                placeholder="Select Date and Time"
                size="md"
                type="datetime-local"
                onChange={handleChange}
              />
            </div>
            

            <div className="button-container">
              <Button colorScheme="yellow" onClick={handleSubmit}>
                SUBMIT
              </Button>
            </div>
          </div>
        ) : (
          <div className="student-table-container">
            <TableContainer>
              <Table size="sm">
                <Thead>
                  <Tr>
                    <Th>Si. No.</Th>
                    <Th>Name</Th>
                    <Th>Email</Th>
                    <Th>Salary</Th>
                    <Th>Date of Join</Th>
                    <Th>Phone</Th>
                    <Th>Course Id</Th>
                    <Th>Staff Id</Th>
                  </Tr>
                </Thead>
                <Tbody>
                {
                    staffList?.length!==0?
                    staffList?.map((item,i)=>(
                     <Tr key={i}>
                        <Td>{i+1}</Td>
                        <Td>{item?.name}</Td>
                        <Td>{item?.email}</Td>
                        <Td>{item?.salary}</Td>
                        <Td>{item?.dateOfJoined}</Td>
                        <Td>{item?.phone}</Td>
                        <Td>{item?.courseId}</Td>
                        <Td>{item?.staffId}</Td>
                        <Td>
                          
                        </Td>
                     </Tr>
                    )):
                    <div style={{textAlign:'center',color:'white'}}>No Data Found!</div>
                 }
                </Tbody>
              </Table>
            </TableContainer>
          </div>
        )}
      </div>
    </div>
  );
};

export default Student;
