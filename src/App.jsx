import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import Student from "./pages/Student/Student";
import Staff from "./pages/Staff/Staff";
import Course from "./pages/Course/Course";
import Home from "./pages/Home/Home";
import { ChakraProvider } from '@chakra-ui/react'
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <ChakraProvider>
    <Router>
      <div style={{display:'flex'}}>
      <Navbar/>
      <div className="second_container">   
      <Routes>
      <Route path="/" element={<Home/>} />
        <Route path="/student" element={<Student/>} />
        <Route path="/staff" element={<Staff/>} />
        <Route path="/course" element={<Course/>} />
      </Routes>
      </div>
      </div>
     
    </Router>
    </ChakraProvider>
  );
}

export default App;
