import React, { useState } from 'react'
import { v4 as uuid } from 'uuid'

const useRandomID = () => {
  return uuid() + Date.now()
}

export default useRandomID
