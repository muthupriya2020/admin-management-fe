import { ENDPOINTS } from "../../constants/endpoints"
import Axios from '../../api-instance'


//For course
export const createCourse = async (userData) => {
  const createCourseData = await Axios.post(ENDPOINTS.CREATECOURSE, userData)
  return createCourseData.data
}


export const listCourse = async (userData) => {
  const listCourseData = await Axios.get(ENDPOINTS.LISTCOURSE)
  return listCourseData.data
}


export const editCourse = async (userData) => {
  const editCourseData = await Axios.post(ENDPOINTS.EDITCOURSE, userData)
  return editCourseData.data
}

export const deleteCourse = async (userData) => {
  const deleteCourseData = await Axios.post(ENDPOINTS.DELETECOURSE, userData)
  return deleteCourseData.data
}


//For Student
export const createStudent = async (userData) => {
  const createStudentData = await Axios.post(ENDPOINTS.CREATESTUDENT, userData)
  return createStudentData.data
}


export const listStudent = async (userData) => {
  const listStudentData = await Axios.get(ENDPOINTS.LISTSTUDENT)
  return listStudentData.data
}


export const editStudent = async (userData) => {
  const editStudentData = await Axios.post(ENDPOINTS.EDITSTUDENT, userData)
  return editStudentData.data
}

export const deleteStudent = async (userData) => {
  const deleteStudentData = await Axios.post(ENDPOINTS.DELETESTUDENT, userData)
  return deleteStudentData.data
}


//For Staff
export const createStaff = async (userData) => {
  const createStaffData = await Axios.post(ENDPOINTS.CREATESTAFF, userData)
  return createStaffData.data
}


export const listStaff = async (userData) => {
  const listStaffData = await Axios.get(ENDPOINTS.LISTSTAFF)
  return listStaffData.data
}


export const editStaff = async (userData) => {
  const editStaffData = await Axios.post(ENDPOINTS.EDITSTAFF, userData)
  return editStaffData.data
}

export const deleteStaff = async (userData) => {
  const deleteStaffData = await Axios.post(ENDPOINTS.DELETESTAFF, userData)
  return deleteStaffData.data
}