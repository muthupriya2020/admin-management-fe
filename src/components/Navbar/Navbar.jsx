import React from 'react'
import './Navbar.css'
import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <div className='navbar-main-container'>
        <div className='navbar-container'>
            <h1 className='side-title'>LOGO</h1>

            <ul className='listes'>
                <li>
                    <Link to="/student">Student</Link>
                </li>
                <li>
                    <Link to="/course">Course</Link>
                </li>
                <li>
                    <Link to="/staff">Staff</Link>
                </li>
            </ul>
        </div>
    </div>
  )
}

export default Navbar