import { BASE_URL } from "./constants/endpoints";
import axios from 'axios'

const instance = axios.create({
    baseURL: BASE_URL,
    timeout: 1000,
    headers: {
        'Content-Type': 'application/json',
        'Content-Type': 'multipart/form-data'
    }
  });

  export default instance;