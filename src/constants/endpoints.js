
const ENDPOINTS = {
"CREATECOURSE":"/course/create",
"LISTCOURSE":"/course/list",
"EDITCOURSE":'course/edit',
"DELETECOURSE":"course/delete",
"CREATESTAFF":"/staff/create",
"LISTSTAFF":"/staff/list",
"EDITSTAFF":'/staff/edit',
"DELETESTAFF":"/staff/delete",
"CREATESTUDENT":"/student/create",
"LISTSTUDENT":"/student/list",
"EDITSTUDENT":'/student/edit',
"DELETESTUDENT":"/student/delete"
}

const BASE_URL = "http://localhost:5000/api/v1"

export {ENDPOINTS, BASE_URL};